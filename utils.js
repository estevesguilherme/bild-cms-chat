const nodemailer = require("nodemailer");
const chatController = require("./controllers/chatController");
const moment = require('moment');

module.exports = {
    sendEmail: sendEmail
};

function sendEmail(idTalk) {

    chatController.getById(idTalk, (result) => {
        let talk = result[0];
        let templateEmail = getTemplateEmail(talk);

        var transporter = nodemailer.createTransport({
            host: "smtp-mail.outlook.com",
            port: 587,
            secure: false,
            auth: {
                user: "guilhermeesteves@outlook.com.br",
                pass: "123"
            }
        });

        var mailOptions = {
            from: '"Bild Chat" <guilhermeesteves@outlook.com.br>',
            to: talk.userEmail,
            subject: `✔ Bild - Atendimento Chat - ${talk.id} ✔`,
            html: templateEmail
        }

        transporter.sendMail(mailOptions, function (err, info) {
            if (err) {
                console.log(err);
            } else {
                console.log("Mensagem enviada com sucesso");
            }
        });
    });
}

function getTemplateEmail(talk) {
    let templateMessages = '';

    talk.messages.forEach((message) => {
        let remetent = message.idUser 
            ? `Usuario ( ${talk.userName} ) ` : `Atendente ( ${talk.attendantName} ) `;
        templateMessages +=
            `<p>${remetent}: ${message.text}</p>`;
    });

    return `<html>
                <body>
                    <h3>Atendimento realizado em - 
                        ${moment(talk.talkDate).format('DD/MM/YYYY HH:mm:ss')}</h3>
                    <h4>Mensagens:</h4>
                    ${templateMessages}
                </body>
            </html>`;
}