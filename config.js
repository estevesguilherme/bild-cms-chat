const mysql = require('mysql');

module.exports = {
    getConnectionDataBase: () => {
        return mysql.createConnection({
            host: "localhost",
            database: "starterl3",
            user: "root",
            password: "123"
        });
    }
}