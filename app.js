const express = require('express');
const moment = require('moment');
var mysql = require('mysql');
const utils = require('./utils');
const app = express();
const server = require('http').createServer(app);
const io = require('socket.io')(server, { pingTimeout: 30000 });
const port = process.env.PORT || 3000;

app.use(express.static(require('path').join(__dirname, '/frontend')));

app.set('views', __dirname + '/frontend');
app.set('view engine', 'vash');
app.engine('html', require('vash').renderFile);
global.lerry = [{ nome: "esteves" }];

(function loadJsonBodyParser() {
    var bodyParser = require('body-parser');
    app.use(bodyParser.urlencoded({ limit: '20mb', extended: true }));
    app.use(bodyParser.json());
})();

//Quado implementar controller e routes 
(function loadModelViewControllers() {
    require('express-load')('controllers')
        .then('routes')
        .into(app);
})();

const chatController = app.controllers.chatController;

server.listen(port, function () {
    console.log('Server listening at port %d', port);
});

global.usersSocket = [];
global.attendantsSocket = [];
global.talks = [];

io.on('connection', function (socket) {
    if (socket.handshake.query['idUser'] &&
        usersSocket.filter(function (e) {
            return e.idUser == socket.handshake.query['idUser']
        }).length == 0) {
        console.log('Conectado User');
        usersSocket.push({
            user: {
                id: socket.handshake.query['idUser'],
                name: socket.handshake.query['nameUser'],
                connectionDate: moment().format('HH:mm:ss'),
                userEmail: socket.handshake.query['emailUser'],
                userName: socket.handshake.query['nameUser'],
                showInQueue: true
            },
            socket
        });
    } else
        if (socket.handshake.query['idAttendant'] &&
            attendantsSocket.filter(function (e) {
                return e.idAttendant == socket.handshake.query['idAttendant']
            }).length == 0) {
            console.log('Conectado Atendente');
            attendantsSocket.push({
                idAttendant: socket.handshake.query['idAttendant'],
                attendantName: socket.handshake.query['attendantName'],
                socket
            });
            socket.emit('refreshTalksAttendant', getTalksAttendant(socket.handshake.query['idAttendant']));
        }

    io.sockets.emit('refreshQueueUser', getUsersOnline());

    socket.on('sendMessageUser', function (data) {
        getTalkUser(data, (talkUser) => {

            let message = {
                idChatTalk: talkUser.id,
                idUser: data.idUser,
                idAttendant: null,
                text: data.message,
                date: moment().format('YYYY-MM-DD HH:mm:ss')
            };

            talkUser.messages.push(message);
            chatController.insertMessage(message, (err, resultMessage) => {
                io.sockets.emit('refreshQueueUser', getUsersOnline());

                let socketAttendant =
                    attendantsSocket.filter((item) => item.idAttendant == talkUser.idAttendant);

                if (!socketAttendant.length)
                    return;

                io.sockets.connected[socketAttendant[0].socket.id].emit('refreshMessage', message);
            })
        });
    });

    socket.on('sendMessageAttendant', function (data) {
        getTalkUser(data, (talkUser) => {
            let message = {
                idChatTalk: talkUser.id,
                idAttendant: talkUser.idAttendant,
                idUser: null,
                text: data.text,
                date: moment().format('YYYY-MM-DD HH:mm:ss'),
                attendantName:
                attendantsSocket.filter((e) => { return e.idAttendant == talkUser.idAttendant })
                [0].attendantName
            };

            talkUser.messages.push(message);

            chatController.insertMessage(message, (err, resultMessage) => {

                if (err) {
                    console.log('Err sendMessageAttendant:', err);
                    return;
                }

                let socketUser =
                    usersSocket.filter((item) => item.user.id == talkUser.idUser);

                if (!socketUser.length)
                    return;

                io.sockets.connected[socketUser[0].socket.id].emit('refreshMessage', message);
            });
        });
    });

    socket.on('checkTalkUser', function (idAttendant, idUser) {
        if (talks.filter((t) => { return t.idUser == idUser && t.idAttendant == idAttendant }).length) {
            return;
        }

        let talkFilter = talks.filter((t) => { return t.idUser == idUser })[0];

        talkFilter.idAttendant = idAttendant;
        talkFilter.attendantName =
            attendantsSocket.filter((e) => { return e.idAttendant == idAttendant })[0].attendantName;
        talkFilter.attendantActive = true;

        chatController.update(talkFilter, (err, result) => {
            if (err) {
                console.log('Err update checkTalkUser', err);
                return;
            }

            usersSocket.filter((item) => { return item.user.id == idUser })[0].user.showInQueue = false;
            io.sockets.emit('refreshQueueUser', getUsersOnline());

            socket.emit('refreshTalksAttendant', getTalksAttendant(idAttendant));
        });
    });

    socket.on('forceDisconnect', function () {
        socket.disconnect();
    });

    socket.on('disconnect', function () {
        let userSocket = usersSocket.filter(function (e) { return e.socket.id == socket.id });
        if (userSocket.length) {
            usersSocket = usersSocket.filter(function (e) { return e.socket.id != socket.id });

            let talkUser = talks.filter((t) => {
                return t.idUser == userSocket[0].user.id && t.idAttendant != null
            });
            if (talkUser.length) {
                talkUser[0].userActive = false;

                chatController.update(talkUser[0], (err, result) => {
                    if (err) {
                        console.log('Err disconect user: ', err);
                        return;
                    }

                    let attenSocket = attendantsSocket.filter((attendentSocket) => {
                        return attendentSocket.idAttendant == talkUser[0].idAttendant
                    })[0];

                    let talksUserNow = getTalksAttendant(talkUser[0].idAttendant);

                    io.sockets.connected[attenSocket.socket.id]
                        .emit('refreshTalksAttendant', talksUserNow);

                    io.sockets.emit('refreshQueueUser', getUsersOnline());
                });
            }
            console.log('Saiu um usuario');
        }

        let attendantSocket = attendantsSocket.filter(function (e) { return e.socket.id == socket.id });
        if (attendantSocket.length) {
            attendantsSocket = attendantsSocket.filter(function (e) { return e.socket.id != socket.id });
            console.log('Saiu um atendente');
        }

        io.sockets.emit('refreshQueueUser', getUsersOnline());
    });

    socket.on('returnUserQueue', function (idUser) {
        let talkFilter = talks.filter((t) => { return t.idUser == idUser })[0];
        talkFilter.idAttendant = null;
        talkFilter.attendantName = null;
        usersSocket.filter((userSocket) => { return userSocket.user.id == idUser })
        [0].user.showInQueue = true;

        chatController.update(talkFilter, (err, result) => {
            if (err) {
                console.log('Err returnUserQueue', err);
                return;
            }
            io.sockets.emit('refreshQueueUser', getUsersOnline());
        });
    });

    socket.on('finishUserChat', (idUser) => {
        let talkFinished = talks.filter((t) => { return t.idUser == idUser });
        talkFinished[0].finishedDate = moment().format('YYYY-MM-DD HH:mm:ss');

        chatController.update(talkFinished[0], (err, result) => {
            if (err) {
                console.log('Err finishUserChat', err);
                return;
            }

            talks = talks.filter((t) => { return t.idUser != idUser });

            let userSocket = usersSocket
                .filter((userSocket) => { return userSocket.user.id == idUser });

            if (userSocket.length) {
                usersSocket
                    .filter((userSocket) => { return userSocket.user.id == idUser })[0]
                    .socket.disconnect();
            }

            io.sockets.emit('refreshQueueUser', getUsersOnline());
        });
    });

    socket.on('finishUserChatWithEmail', (idUser) => {
        var talkFinished = talks.filter((t) => { return t.idUser == idUser });
        talkFinished[0].finishedDate = moment().format('YYYY-MM-DD HH:mm:ss');

        chatController.update(talkFinished[0], (err, result) => {
            if (err) {
                console.log('Err finishUserChatWithEmail', err);
                return;
            }

            talks = talks.filter((t) => { return t.idUser != idUser });

            let userSocket = usersSocket
                .filter((userSocket) => { return userSocket.user.id == idUser });

            if (userSocket.length) {
                usersSocket
                    .filter((userSocket) => { return userSocket.user.id == idUser })[0]
                    .socket.disconnect();
            }

            utils.sendEmail(talkFinished[0].id);

            io.sockets.emit('refreshQueueUser', getUsersOnline());
            //Disparar email;
        });
    });


});

function getUsersOnline() {
    let t = usersSocket.filter((item) => {
        return item.user.showInQueue &&
            talks.filter(t => { return t.idUser == item.user.id }).length
    }).map((userSocket) => {
        return userSocket.user;
    });

    return t.sort((a, b) => {return a.connectionDate > b.connectionDate});
}

function getTalksAttendant(idAttendant) {
    return talks.filter((talk) => {
        return !talk.finishedDate && talk.idAttendant == idAttendant;
    });
}

function getTalkUser(data, callback) {
    let talk = talks.filter(talk => { return talk.idUser == data.idUser });
    if (talk.length)
        callback(talk[0]);

    if (!talks.filter(talk => { return talk.idUser == data.idUser }).length) {
        let userFilter = usersSocket.filter(item => { return item.user.id == data.idUser })[0].user;
        let talkNew = {
            idAttendant: null,
            idUser: data.idUser,
            userName: userFilter.name,
            userEmail: userFilter.userEmail,
            talkDate: moment().format('YYYY-MM-DD HH:mm:ss'),
            userActive: true,
            attendantActive: false,
            attendantName: null,
            messages: [],
            finishedDate: null
        };

        chatController.insert(talkNew, (err, result) => {
            talkNew.id = result.insertId;
            talks.push(talkNew);
            callback(talks.filter(talk => { return talk.idUser == data.idUser })[0]);
        });
    }
}

function init() {
    chatController.getAll((result) => {
        result.forEach((talk) => {
            let userSocket = usersSocket.filter((u) => { return u.user.id == talk.idUser });
            if (!userSocket.length) {
                talk.finishedDate = moment().format('YYYY-MM-DD HH:mm:ss');
                chatController.update(talk, (err, result) => {
                    if (err) {
                        console.log('erro update talk', err);
                    }
                });
                utils.sendEmail(talk.id);
                return;
            }

            talks.push(talk);
            userSocket[0].user.connectionDate = moment(talk.talkDate).format('HH:mm:ss');

            let attendant = attendantsSocket.filter((a) => { return a.idAttendant == talk.idAttendant });
            if (!attendant.length) {
                talk.idAttendant = null;
                userSocket[0].user.showInQueue = true;
                return;
            }

            talk.userName = usersSocket.filter(item => { return item.user.id == talk.idUser })
            [0].user.name;
            userSocket[0].user.showInQueue = false;
            io.sockets.connected[attendant[0].socket.id]
                .emit('refreshTalksAttendant', getTalksAttendant(attendant[0].idAttendant))
        });

        io.sockets.emit('refreshQueueUser', getUsersOnline());
        console.log('Restore OK');
    });
}

setTimeout(() => {
    init();
    checkAttendantExit();
}, 10000);

function checkAttendantExit() {
    setInterval(() => {
        talks.forEach((t) => {
            if (!attendantsSocket.filter((a) => { return a.idAttendant == t.idAttendant }).length) {
                if (!attendantsSocket.filter((a) => { return a.idAttendant == t.idAttendant }).length) {
                    t.idAttendant = null;
                    t.attendantName = null;
                    usersSocket.filter((su) =>
                    { return su.user.id == t.idUser })[0].user.showInQueue = true;
                    io.sockets.emit('refreshQueueUser', getUsersOnline());
                }
            }
        });
    }, 5000)
}