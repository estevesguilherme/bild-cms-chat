const con = require('./../config').getConnectionDataBase();
const moment = require('moment');

module.exports = {
    getAll: getAll,
    insert: insert,
    insertMessage: insertMessage,
    update: update,
    getById: getById
};

var talksDb = [];

function getAll(callback){
    con.query('select * from chat_talk where finishedDate is null', (err,result) =>{
        talksDb = result;
        getMessageTalkRecursive(0,callback,con);
    });
}

function getById(idTalk, callback){
    con.query(`select * from chat_talk where id = ${idTalk}`, (err,result) =>{
        talksDb = result;
        getMessageTalkRecursive(0,callback,con);
    });
}

function insert(talk, callback) {
    let sql =
        `insert into chat_talk(idAttendant,idUser,talkDate,userActive,attendantActive,userName,
                userEmail,attendantName, finishedDate) 
                values(${talk.idAttendant},${talk.idUser},'${talk.talkDate}',
                    ${talk.userActive},${talk.attendantActive},'${talk.userName}',
                    '${talk.userEmail}',${talk.attendantName ? "'" + talk.attendantName + "'" : null},
                    ${talk.finishedDate ? "'" + talk.finishedDate + "'" : null})`;

    con.query(sql, (err, result)=>{
        callback(err,result);
    });
}

function insertMessage(message, callback) {
    let sql =
        `insert into chat_talk_message(idChatTalk,idAttendant,idUser,text,date) 
                values(${message.idChatTalk},${message.idAttendant},
                    ${message.idUser},'${message.text}','${message.date}')`;

    con.query(sql, (err, result)=>{
        callback(err,result);
    });
}

function getMessageTalkRecursive(index,callback,c){
    if(index == talksDb.length){
        callback(talksDb);
        return;
    }
    let sql = `select * from chat_talk_message where idChatTalk = ${talksDb[index].id}`;
    c.query(sql, (err,result) => {
        talksDb[index].messages = result;
        getMessageTalkRecursive(++index,callback,c);
    });
};

function update(talk,callback){
    let sql =
        `update chat_talk set idAttendant=${talk.idAttendant},idUser=${talk.idUser},
            userActive=${talk.userActive},attendantActive=${talk.attendantActive},
            userName='${talk.userName}',userEmail='${talk.userEmail}',
            attendantName=${talk.attendantName ? "'" + talk.attendantName + "'" : null},
            finishedDate=${talk.finishedDate ? "'" + talk.finishedDate + "'" : null}
            where id=${talk.id}`; 

    con.query(sql, (err, result)=>{
        callback(err,result);
    });
}