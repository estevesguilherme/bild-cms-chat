create table chat_talk(
	id int primary key auto_increment,
	idAttendant int null,
	idUser int null,
	talkDate datetime not null,
	userActive bool null,
	attendantActive bool null,
	userEmail varchar(100) null,
	userName varchar(100) null,
	attendantName varchar(100) null,
	finishedDate datetime null
);

create table chat_talk_message(
	id int primary key auto_increment,
	idChatTalk int not null,
	idAttendant int null,
	idUser int null,
	text varchar(2000) null,
	date datetime not null,
	foreign key (idChatTalk) references chat_talk(id)
);
